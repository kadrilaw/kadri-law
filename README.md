Eddie H. Kadri, B.A, JD., Esq. is an immigration lawyer who has been practicing law in the Province of Ontario for more than 16 years. He is widely recognized in the global business community as a leading professional in corporate immigration law and related business/trade matters.

Address: 110 Tecumseh Rd E, #200, Windsor, ON N8X 2P8, Canada

Phone: 519-258-8188

Website: https://kadrilaw.com/
